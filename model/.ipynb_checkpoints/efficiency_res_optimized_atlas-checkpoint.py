import numpy as np
from typing import NamedTuple
from collections import Counter
from math import sqrt as sqrt
import awkward as ak

class ValueSet_res(NamedTuple):
    """
    Class to represent a results tuple. Adding and printing supported,
    along with a few convenience properties.
    """

    S: int
    MT: int
    FP: int
    events: int = 1

    @property
    def real_pvs(self):
        return self.S + self.MT

    @property
    def eff_rate(self):
        if self.real_pvs==0:
            return self.real_pvs
        else:
            return self.S / self.real_pvs

    @property
    def fp_rate(self):
        return self.FP / self.events

    def __repr__(self):
        message = f"Found {self.S} of {self.real_pvs}, added {self.FP} (eff {self.eff_rate:.2%})"
        if self.events > 1:
            message += f" ({self.fp_rate:.3} FP/event)"
        #if self.S != self.Sp:
        #    message += f" ({self.S} != {self.Sp})"
        return message

    def __add__(self, other):
        return self.__class__(
            self[0] + other[0],
            self[1] + other[1],
            self[2] + other[2],
            self[3] + other[3],
        )

    def pretty(self):
        s_message = f"Successes: {self.S:,}"
        return f"""\        
Real PVs in validation set: {self.real_pvs:,}
{s_message}
Missed true PVs: {self.MT:,}
False positives: {self.FP:,}
Efficiency of detecting real PVs: {self.eff_rate:.2%}
False positive rate: {self.fp_rate:.3}"""

    


def pv_locations_updated_res(
    targets,
    threshold,
    integral_threshold,
    min_width
):
    """
    Compute the z positions from the input KDE using the parsed criteria.
    
    Inputs:
      * targets: 
          Numpy array of KDE values (predicted or true)

      * threshold: 
          The threshold for considering an "on" value - such as 1e-2

      * integral_threshold: 
          The total integral required to trigger a hit - such as 0.2

      * min_width: 
          The minimum width (in bins) of a feature - such as 2

    Returns:
      * array of float32 values corresponding to the PV z positions
      
    """
    # Counter of "active bins" i.e. with values above input threshold value
    state = 0
    # Sum of active bin values
    integral = 0.0
    # Weighted Sum of active bin values weighted by the bin location
    sum_weights_locs = 0.0
    # keeps track of peak location (assuming first entry is close to zero)
    currentmax = 0

    # Make an empty array and manually track the size (faster than python array)
    items = np.empty(500, np.float32)
    peakpos = np.empty(500, np.int32)
    conjoinedleft = np.empty(500, bool)
    conjoinedright = np.empty(500, bool)
    # Number of recorded PVs
    nitems = 0

    # Account for special case where two close PV merge KDE so that
    # targets[i] never goes below the threshold before the two PVs are scanned through
    peak_passed = False
    
    # Loop over the bins in the KDE histogram
    for i in range(len(targets)):
#         print("i = ", i, ", value = ", targets[i], ", state = ", state, ", currentmax = ", currentmax)
        if state==0:
            currentmax = i
        # If bin value above 'threshold', then trigger
        if targets[i] >= threshold:
            state += 1
            integral += targets[i]
            sum_weights_locs += i * targets[i]  # weight times location
            
            if targets[i]>targets[currentmax]: currentmax = i
            
            if targets[i-1]>targets[i]:
                peak_passed = True # keeps track of whether we passed peak in predicted distribution
            
        if (targets[i] < threshold or i == len(targets) - 1 or (targets[i-1]<targets[i] and peak_passed)) and state > 0:
            #if (targets[i] < threshold or i == len(targets) - 1) and state > 0:

            # Record a PV only if 
            if state >= min_width and integral >= integral_threshold:
                # Adding '+0.5' to account for the bin width 
                items[nitems] = (sum_weights_locs / integral) + 0.5 
                
                peakpos[nitems] = currentmax

                if (targets[i-1]<targets[i] and peak_passed): conjoinedleft[nitems] = True
                else: conjoinedleft[nitems] = False
                    
                if nitems>0 and conjoinedleft[nitems-1]==True: conjoinedright[nitems] = True
                else: conjoinedright[nitems] = False
                    
                nitems += 1
                
            # reset state
            state = 0
            integral = 0.0
            sum_weights_locs = 0.0
            peak_passed=False
            

    # Special case for final item (very rare or never occuring)
    # handled by above if len
    return items[:nitems], peakpos[:nitems], conjoinedleft[:nitems], conjoinedright[:nitems]
#####################################################################################

#####################################################################################


def filter_nans_res(
    items,
    mask
):
    """
    Method to mask bins in the predicted KDE array if the corresponding bin in the true KDE array is 'nan'.
    
    Inputs:
      * items: 
          Numpy array of predicted PV z positions
      * mask: 
          Numpy array of KDE values (true PVs)

    Returns:
      * Boolean-valued Numpy array corresponding to which items are valid
      
    """
    # Create array corresponding to items (will say which items are valid, aka not masked)
    validinds = np.zeros(items.shape, dtype=bool)
    # Loop over the predicted PV z positions
    for i, item in enumerate(items):
        index = int(round(item))
        not_valid = np.isnan(mask[index])
        if not not_valid:
            validinds[i] = True

    return validinds
#####################################################################################



def get_reco_resolution(
    pred_PVs_loc,
    pred_PVs_peakloc,
    pred_PVs_cleft,
    pred_PVs_cright,
    predict,
    nsig_res,
    steps_extrapolation,
    ratio_max,
    debug
):
    """
    Compute the resolution as a function of predicted KDE histogram 

    Inputs:
      * pred_PVs_loc: 
          Numpy array of computed z positions of the predicted PVs (using KDEs)

      * predict: 
          Numpy array of predictions

      * nsig_res: 
          Empirical value representing the number of sigma wrt to the std resolution 
          as a function of FWHM

      * threshold: 
          The threshold for considering an "on" value - such as 1e-2

      * integral_threshold: 
          The total integral required to trigger a hit - such as 0.2

      * min_width: 
          The minimum width (in bins) of a feature - such as 2

      * debug: 
          flag to print output for debugging purposes


    Ouputs: 
        Numpy array of filtered and sorted (in z values) expected resolution on the reco PVs z position.
    """
    
    #    # Get the z position from the predicted KDEs distribution
    #    predict_values = pv_locations_updated_res(predict, threshold, integral_threshold, min_width)

    
    # # Using the filter_nans_res method to 'mask' the bins in 'predict_values' 
    # # where the corresponding bins in truth are 'nan' 
    # filtered_predict_values = filter_nans_res(predict_values, truth)

    reco_reso = np.empty_like(pred_PVs_loc)

    steps = steps_extrapolation
    
    i_predict_pv=0
        
    if steps==0:

        # This is for the case where we do not extrapolate values in between bins
        for i, predict_pv in enumerate(pred_PVs_loc):
            predict_pv_KDE_max = predict[pred_PVs_peakloc[i]]

            FWHM = ratio_max*predict_pv_KDE_max

            ibin_min = -1
            ibin_max = -1
            
            for ibin in range(pred_PVs_peakloc[i],pred_PVs_peakloc[i]-20,-1):
                predict_pv_KDE_val = predict[ibin]
                if predict_pv_KDE_val<FWHM:
                    ibin_min = ibin
                    break

            for ibin in range(pred_PVs_peakloc[i],pred_PVs_peakloc[i]+20):
                predict_pv_KDE_val = predict[ibin]
                if predict_pv_KDE_val<FWHM:
                    ibin_max = ibin
                    break
                    
            if pred_PVs_cright[i]==False and pred_PVs_cleft[i]==True: # if a conjoined PV exists to the right
                FWHM_w = 2*(pred_PVs_peakloc[i] - ibin_min)
            if pred_PVs_cright[i]==True and pred_PVs_cleft[i]==False: # if a conjoined PV exists to the left
                FWHM_w = 2*(ibin_max - pred_PVs_peakloc[i])
            else: FWHM_w = (ibin_max-ibin_min)
                
            standard_dev = FWHM_w/2.335
            reco_reso[i_predict_pv] = nsig_res*standard_dev
            i_predict_pv+=1
                
    else:
        
        for i, predict_pv in enumerate(pred_PVs_loc):
            predict_pv_KDE_max = predict[pred_PVs_peakloc[i]]

            FWHM = ratio_max*predict_pv_KDE_max

            ibin_min_extrapol = -1
            ibin_max_extrapol = -1
            found_min = False
            found_max = False
            for ibin in range(pred_PVs_peakloc[i],max(pred_PVs_peakloc[i]-20,1),-1):
                if not found_min:
                    predict_pv_KDE_val_ibin = predict[ibin]
                    predict_pv_KDE_val_prev = predict[ibin-1]

                    # Apply a dummy linear extrapolation between the two neigbour bins values 
                    delta_steps = (predict_pv_KDE_val_prev - predict_pv_KDE_val_ibin)/steps
                    for sub_bin in range(int(steps)):
                        predict_pv_KDE_val_ibin -= delta_steps*sub_bin

                        if predict_pv_KDE_val_ibin<FWHM:
                            ibin_min_extrapol = (ibin*steps-sub_bin)/steps
                            found_min=True

            for ibin in range(pred_PVs_peakloc[i],min(pred_PVs_peakloc[i]+20,11999)):
                if not found_max:
                    predict_pv_KDE_val_ibin = predict[ibin]
                    predict_pv_KDE_val_next = predict[ibin+1]

                    # Apply a dummy linear extrapolation between the two neigbour bins values 
                    delta_steps = (predict_pv_KDE_val_ibin - predict_pv_KDE_val_next)/steps
                    for sub_bin in range(int(steps)):
                        predict_pv_KDE_val_ibin -= delta_steps*sub_bin

                        if predict_pv_KDE_val_ibin<FWHM:
                            ibin_max_extrapol = (ibin*steps+sub_bin)/steps
                            found_max=True
                              
            if not found_max: ibin_max_extrapol=12000
            if not found_min: ibin_min_extrapol=0
            

            if pred_PVs_cright[i]==False and pred_PVs_cleft[i]==True: # if a conjoined PV exists to the right
                FWHM_w = 2*(pred_PVs_peakloc[i] - ibin_min_extrapol)
            if pred_PVs_cright[i]==True and pred_PVs_cleft[i]==False: # if a conjoined PV exists to the left
                FWHM_w = 2*(ibin_max_extrapol - pred_PVs_peakloc[i])
            else: FWHM_w = (ibin_max_extrapol - ibin_min_extrapol)
            
            standard_dev = FWHM_w/2.335
            reco_reso[i_predict_pv] = nsig_res*standard_dev
            i_predict_pv+=1
        
    return reco_reso
#####################################################################################

    

#####################################################################################


def get_PVs_label(
    get_Preds,
    truth, 
    predict, 
    min_res, 
    threshold, 
    integral_threshold, 
    min_width, 
    nsig_res_FWHM, 
    steps_extrapolation, 
    ratio_max,
    debug
):
    """
    Method to obtain the PVs labels (i.e. list of true PV that are matched to predicted PV, or vice-versa).

    Inputs:
      * truth: 
          Numpy array of truth values

      * predict: 
          Numpy array of predictions

      * min_res: 
          Minimal resolution value (in terms of bins) for the search window - such as 3

      * threshold: 
          The threshold for considering an "on" value - such as 1e-2

      * integral_threshold: 
          The total integral required to trigger a hit - such as 0.2

      * min_width: 
          The minimum width (in bins) of a feature - such as 2
          
      * nsig_res_FWHM: 
          Number of sigmas to use to calculate resolution
          
      * steps_extrapolation: 
          Number of steps to extrapolate when calculating the FWHM (0 means no extrapolation)
          
      * ratio_max: 
          Ratio of the half maximum to take (usually 0.5)

      * debug: 
          flag to print output for debugging purposes


    Ouputs: 
        Numpy array of '0=not matched' or '1=matched' acting like booleans.
    """
    
    # Get the z position from the true KDEs distribution (EMK get rid of this and just use truth locations)
    target_PVs_loc, _ = pv_locations_updated_res(truth, threshold, integral_threshold, min_width)
    # Get the z position from the predicted KDEs distribution
    pred_PVs_loc, pred_PVs_peakloc, pred_PVs_cleft, pred_PVs_cright = pv_locations_updated_res(predict, threshold, integral_threshold, min_width)

    # Using the filter_nans_res method to mask the PVs in 'pred_PVs_loc' 
    # where the corresponding bins in truth are 'nan' 
    validind = filter_nans_res(pred_PVs_loc, truth)
    filtered_pred_PVs_loc = pred_PVs_loc[validind]
    filtered_pred_PVs_peakloc = pred_PVs_peakloc[validind]
    filtered_pred_PVs_cleft = pred_PVs_cleft[validind]
    filtered_pred_PVs_cright = pred_PVs_cright[validind]

    # Get the true PV resolutions, sorted by ascending z value position
    # The sorting in z values is important, because the arrays target_PVs_loc 
    # and pred_PVs_loc obtained from 'pv_locations_updated_res' are sorted by ascending z values 
    # (by construction from the KDEs histograms)
    filtered_and_sorted_res = get_reco_resolution(filtered_pred_PVs_loc,
                                                  filtered_pred_PVs_peakloc,
                                                  filtered_pred_PVs_cleft,
                                                  filtered_pred_PVs_cright,
                                                  predict, 
                                                  nsig_res_FWHM, 
                                                  steps_extrapolation, 
                                                  ratio_max, 
                                                  debug)
    
    
    if get_Preds==0:
        # Initialize the array to an array of zeros with target_PVs_loc shape 
        PVs_label = np.zeros(target_PVs_loc.shape,dtype=int)
        # Loop over the true PVs
        for i in range(len(target_PVs_loc)):
            # Get the window of interest: [min_val, max_val] 
            # The window is obtained from the value of z of the true PV 'i'
            # +/- the resolution as a function of the number of tracks for the true PV 'i'
            min_val = target_PVs_loc[i]-filtered_and_sorted_res[i]
            max_val = target_PVs_loc[i]+filtered_and_sorted_res[i]
            # Loop over the 'filtered' predicted PVs
            for j in range(len(filtered_pred_PVs_loc)):                
                if min_val <= filtered_pred_PVs_loc[j] and filtered_pred_PVs_loc[j] <= max_val:
                    # If condition is met, then the element 'i' 
                    # of the PVs_label array is set to '1' 
                    PVs_label[i] = 1
                    # the predicted PV is removed from the original array to avoid associating 
                    # one true PV to multiple predicted PVs
                    # (this could happen for PVs with close z values)
                    filtered_pred_PVs_loc = np.delete(filtered_pred_PVs_loc,[j])
                    # Since a true PV and a predicted PV where matched, go to the next true PV 'i'
                    break
                else:
                    # In case, no predicted PV could be associated with the true PV 'i'
                    # then PVs_label[i] is set to '0'
                    PVs_label[i] = 0
        return PVs_label           
    
    elif get_Preds==1:
        PVs_label = np.zeros(filtered_pred_PVs_loc.shape,dtype=int)
        # Loop over the 'filtered' predicted PVs
        for i in range(len(filtered_pred_PVs_loc)):

            # Loop over the true PVs
            for j in range(len(target_PVs_loc)):                

                # Get the window of interest: [min_val, max_val] 
                # The window is obtained from the value of z of the true PV 'i'
                # +/- the resolution as a function of the number of tracks for the true PV 'i'
                min_val = target_PVs_loc[j]-filtered_and_sorted_res[j]
                max_val = target_PVs_loc[j]+filtered_and_sorted_res[j]

                if min_val <= filtered_pred_PVs_loc[i] and filtered_pred_PVs_loc[i] <= max_val:
                    # If condition is met, then the element 'i' 
                    # of the PVs_label array is set to '1' 
                    PVs_label[i] = 1

                    # The predicted PV is removed from the original array to avoid associating 
                    # one true PV to multiple predicted PVs
                    # (this could happen for PVs with close z values)
                    target_PVs_loc = np.delete(target_PVs_loc,[j])
                    # also remove the associated resolution to avoid mis-matching the array dimensions
                    filtered_and_sorted_res = np.delete(filtered_and_sorted_res,[j])
                    
                    # Since a true PV and a predicted PV where matched, go to the next true PV 'i'
                    break
                else:
                    # In case, no predicted PV could be associated with the true PV 'i'
                    # then PVs_label[i] is set to '0'
                    PVs_label[i] = 0
        return PVs_label           

    else:
        PVs_label = np.zeros(filtered_pred_PVs_loc.shape,dtype=int)
        print("Wrong value for the first argument in get_PVs_label")
        print("Needs to be either 0 (get true PV labels) or 1 (predicted PV labels)")
        
        return PVs_label
#####################################################################################

from collections import namedtuple
PerformanceInfo = namedtuple("PerformanceInfo", ("reco_merged",
                                                 "reco_split",
                                                 "reco_clean",
                                                 "reco_fake"))

def compare_res_reco(
    target_PVs_loc,
    pred_PVs_loc,
    reco_res,
    debug
):
    """

    Inputs argument:
      * target_PVs_loc: 
          Numpy array of z positions (in terms of bin #) of the TruthVertex objects. ensure that the list is filtered to require ntrks>=4

      * pred_PVs_loc: 
          Numpy array of computed z positions (in terms of bin #) of the predicted PVs (using KDEs)

      * reco_res: 
          Numpy array with the "reco" resolution as a function of width of predicted KDE signal 

      * debug: 
          flag to print output for debugging purposes
    
    
    Returns:
        PerformanceInfo named tuple
    """
    
    truth_classification = [[]]*len(target_PVs_loc)
    reco_classification = [[]]*len(pred_PVs_loc)
    truth_assignment = [[]]*len(target_PVs_loc)
    reco_assignment = [[]]*len(pred_PVs_loc)
    
    num_merged = []
    
    # iterate through predicted PV locations
    for i, pred_loc in enumerate(pred_PVs_loc):
        
                
        # get all truth vertices within (pred_loc-res,pred_loc+res)
        where_truth = np.argwhere(np.abs(target_PVs_loc-pred_loc)<=reco_res[i])
        
        # takes care of all fake predictions
        if len(where_truth)==0:
            reco_classification[i] = reco_classification[i] + ["fake"]
            reco_assignment[i] = reco_assignment[i] + [-1]
                    
        # takes care of sparse truth assigments (no other surrounding vertices)
        if len(where_truth)==1:
            
            where = where_truth[0][0]
            
            reco_classification[i] = reco_classification[i] + ["clean"]
            truth_assignment[where] = truth_assignment[where] + [i]
            truth_classification[where] = truth_classification[where] + ["clean"]
            reco_assignment[i] = reco_assignment[i] + [where]
                        
        current_truth_loc = target_PVs_loc[where_truth]
        
        # takes care of dense cases (merged)
        if len(where_truth)>1:
            num_merged.append(len(where_truth))
            
            reco_classification[i] = reco_classification[i] + ["merged"]
            for j in where_truth:
                reco_assignment[i] = reco_assignment[i] + [j[0]]
                truth_assignment[j[0]] = truth_assignment[j[0]] + [i]
                truth_classification[j[0]] = truth_classification[j[0]] + ["merged"]
                    
    # take care of remaining missing truth PVs
    for i in range(len(truth_classification)):
        if len(truth_classification[i])==0:
            truth_assignment[i] = truth_assignment[i] + [-1]
            truth_classification[i] = truth_classification[i] + ["missed"]
            
    # handling multiple classifications (split vertices)
    for i in range(len(truth_classification)):
        if len(truth_classification[i])>1:
            
            where_clean = np.argwhere(np.array(truth_classification[i])=="clean")
            where_merged = np.argwhere(np.array(truth_classification[i])=="merged")
            
            # "clean" in list cases
            if len(where_clean)>1:
                    
                # decide which clean assignment is best
                reco_keys = np.array(truth_assignment[i])[np.array(where_clean).reshape((len(where_clean),))]
                diff = np.abs(target_PVs_loc[i] - pred_PVs_loc[reco_keys])/reco_res[reco_keys]
                best_key = reco_keys[np.argmin(diff)]
                    
                # assign split vertices
                for k in reco_keys:
                    if not k==best_key:
                        reco_classification[k] = ["split"]
                        reco_assignment[k] = [i]
                        
            if len(where_merged)>1:
                truth_classification[i] = ["merged"]
                
            if len(truth_classification[i])>1:
                truth_classification[i] = ["merged"]
                          
    # add up each category
    truth_classification = np.array(truth_classification).reshape(len(truth_classification),)
    reco_classification = np.array(reco_classification).reshape(len(reco_classification),)
    # calculates local pileup density
    bins_1mm = 12000/(240 - (-240))
    localdensity = np.zeros(len(target_PVs_loc))
    for i in range(len(target_PVs_loc)):
        localdensity[i] = sum(np.abs(target_PVs_loc[i]-target_PVs_loc)<=bins_1mm)/2
    
    # calculate number of merged, split, fake, clean
    reco_merged = sum(reco_classification=="merged")
    reco_split = sum(reco_classification=="split")
    reco_clean = sum(reco_classification=="clean")
    reco_fake = sum(reco_classification=="fake")
        
    return PerformanceInfo(reco_merged,reco_split,reco_clean,reco_fake), truth_classification, localdensity
    
    
#### LHCb PV-Finder Version ####
def compare_res_reco2(
    target_PVs_loc,
    pred_PVs_loc,
    reco_res,
    debug
):
    """
    Method to compute the efficiency counters: 
    - succeed    = number of successfully predicted PVs
    - missed     = number of missed true PVs
    - false_pos  = number of predicted PVs not matching any true PVs

    Inputs argument:
      * target_PVs_loc: 
          Numpy array of computed z positions of the true PVs (using KDEs)

      * pred_PVs_loc: 
          Numpy array of computed z positions of the predicted PVs (using KDEs)

      * reco_res: 
          Numpy array with the "reco" resolution as a function of width of predicted KDE signal 

      * debug: 
          flag to print output for debugging purposes
    
    
    Returns:
        succeed, missed, false_pos
    """
    
    # Counters that will be iterated and returned by this method
    succeed = 0
    missed = 0
    false_pos = 0
        
    # Get the number of predicted PVs
    len_pred_PVs_loc = len(pred_PVs_loc)
    # Get the number of true PVs 
    len_target_PVs_loc = len(target_PVs_loc)

    # Decide whether we have predicted equally or more PVs than trully present
    # this is important, because the logic for counting the MT an FP depend on this
    if len_pred_PVs_loc >= len_target_PVs_loc:
        if debug:
            print("In len(pred_PVs_loc) >= len(target_PVs_loc)")

        # Since we have N(pred_PVs) >= N(true_PVs), 
        # we loop over the pred_PVs, and check each one of them to decide 
        # whether they should be labelled as S, FP. 
        # The number of MT is computed as: N(true_PVs) - S
        # Here the number of iteration is fixed to the original number of predicted PVs
        for i in range(len_pred_PVs_loc):
            if debug:
                print("pred_PVs_loc = ",pred_PVs_loc[i])
            # flag to check if the predicted PV is being matched to a true PV
            matched = 0

            # Get the window of interest: [min_val, max_val] 
            # The window is obtained from the value of z of the true PV 'j'
            # +/- the resolution as a function of the number of tracks for the true PV 'j'
            min_val = pred_PVs_loc[i]-reco_res[i]
            max_val = pred_PVs_loc[i]+reco_res[i]
            if debug:
                print("resolution = ",(max_val-min_val)/2.)
                print("min_val = ",min_val)
                print("max_val = ",max_val)

            # Now looping over the true PVs.
            for j in range(len(target_PVs_loc)):
                # If condition is met, then the predicted PV is labelled as 'matched', 
                # and the number of success is incremented by 1
                if min_val <= target_PVs_loc[j] and target_PVs_loc[j] <= max_val:
                    matched = 1
                    succeed += 1
                    if debug:
                        print("succeed = ",succeed)
                    # the true PV is removed from the original array to avoid associating 
                    # one predicted PV to multiple true PVs
                    # (this could happen for PVs with close z values)
                    target_PVs_loc = np.delete(np.array(target_PVs_loc),[j])
                    # Since a predicted PV and a true PV where matched, go to the next predicted PV 'i'
                    break
            # In case, no true PV could be associated with the predicted PV 'i'
            # then it is assigned as a FP answer
            if not matched:                
                false_pos +=1
                if debug:
                    print("false_pos = ",false_pos)
        # the number of missed true PVs is simply the difference between the original 
        # number of true PVs and the number of successfully matched true PVs
        missed = (len_target_PVs_loc-succeed)
        if debug:
            print("missed = ",missed)

    else:
        if debug:
            print("In len(pred_PVs_loc) < len(target_PVs_loc)")
        # Since we have N(pred_PVs) < N(true_PVs), 
        # we loop over the true_PVs, and check each one of them to decide 
        # whether they should be labelled as S, MT. 
        # The number of FP is computed as: N(pred_PVs) - S
        # Here the number of iteration is fixed to the original number of true PVs
        for i in range(len_target_PVs_loc):
            if debug:
                print("target_PVs_loc = ",target_PVs_loc[i])
            # flag to check if the true PV is being matched to a predicted PV
            matched = 0
            # Now looping over the predicted PVs.
            for j in range(len(pred_PVs_loc)):                
                # Get the window of interest: [min_val, max_val] 
                # The window is obtained from the value of z of the true PV 'i'
                # +/- the resolution as a function of the number of tracks for the true PV 'i'
                min_val = pred_PVs_loc[j]-reco_res[j]
                max_val = pred_PVs_loc[j]+reco_res[j]
                if debug:
                    print("pred_PVs_loc = ",pred_PVs_loc[j])
                    print("resolution = ",(max_val-min_val)/2.)
                    print("min_val = ",min_val)
                    print("max_val = ",max_val)
                # If condition is met, then the true PV is labelled as 'matched', 
                # and the number of success is incremented by 1
                if min_val <= target_PVs_loc[i] and target_PVs_loc[i] <= max_val:
                    matched = 1
                    succeed += 1
                    if debug:
                        print("succeed = ",succeed)
                    # the predicted PV is removed from the original array to avoid associating 
                    # one true PV to multiple predicted PVs
                    # (this could happen for PVs with close z values)
                    pred_PVs_loc = np.delete(pred_PVs_loc,[j])
                    # Since a predicted PV and a true PV where matched, go to the next true PV 'i'
                    reco_res = np.delete(reco_res,[j])
                    break
            # In case, no predicted PV could be associated with the true PV 'i'
            # then it is assigned as a MT answer
            if not matched:
                missed += 1
                if debug:
                    print("missed = ",missed)
                    
        # the number of false positive predicted PVs is simply the difference between the original 
        # number of predicted PVs and the number of successfully matched predicted PVs
        false_pos = (len_pred_PVs_loc - succeed)
        if debug:
            print("false_pos = ",false_pos)

    return succeed, missed, false_pos
    
    
    
def numba_efficiency_res(
    truth,
    predict,
    true_PVs_nTracks,
    true_PVs_z,
    nsig_res_FWHM,
    steps_extrapolation,
    ratio_max,
    f_ratio_window,
    nbins_lookup,
    min_res,
    threshold,
    integral_threshold,
    min_width,
    debug
):
    """
    Function copied from 'efficiency.py', which now returns 3 values instead of 4. Two values of counted successes are computed in 'efficiency.py', S and Sp, the latter being the number of successes when using the filter_nans method on the true KDE inputs. By default, the number of successes (S in this script) is being computed using the filter_nans_res method.
    """

    # Get the z position from the true KDEs distribution
    true_values = pv_locations_res(truth, threshold, integral_threshold, min_width)
    # Get the z position from the predicted KDEs distribution
    predict_values = pv_locations_res(predict, threshold, integral_threshold, min_width)

    if use_locations_updated:
        # Get the z position from the true KDEs distribution (EMK change to just truth locations)
        true_values, _ = pv_locations_updated_res(truth, threshold, integral_threshold, min_width)
        # Get the z position from the predicted KDEs distribution
        predict_values, predict_peakloc, predict_cleft, predict_cright = pv_locations_updated_res(predict, threshold, integral_threshold, min_width)

        
    # Using the filter_nans_res method to 'mask' the bins in 'predict_values' 
    # where the corresponding bins in truth are 'nan'
    validinds = filter_nans_res(predict_values, truth)
    filtered_predict_values = predict_values[validinds]
    filtered_predict_peakloc = predict_peakloc[validinds]
    filtered_predict_cleft = predict_cleft[validinds]
    filtered_predict_cright = predict_cright[validinds]
    
    
    # ======================================================================
    # Use the resolution as a function of the predicted hist FWHM
    # ======================================================================
        
    # WARNING:  get_reco_resolution needs to take as argument the filtered list of predicted PVs location
    reco_res = get_reco_resolution(filtered_predict_values,
                                   filtered_predict_peakloc,
                                   filtered_predict_cleft,
                                   filtered_predict_cright,
                                   predict, 
                                   nsig_res_FWHM, 
                                   steps_extrapolation, 
                                   ratio_max, 
                                   debug)

    S, MT, FP = compare_res_reco2(true_values, filtered_predict_values, reco_res, debug)
    return S, MT, FP
        
#####################################################################################


def efficiency_res(
    truth,
    predict,
    true_PVs_nTracks,
    true_PVs_z, 
    nsig_res_FWHM,
    steps_extrapolation,
    ratio_max,
    f_ratio_window,
    nbins_lookup,
    min_res,
    threshold,
    integral_threshold,
    min_width,
    debug
):
    """
    Compute three values: The number of succeses (S), the number of missed true
    values (MT), and the number of missed false values (FP). Note that the number of successes
    is computed twice, and both values are returned.

    Inputs:
      * truth: 
          Numpy array of truth values

      * predict: 
          Numpy array of predictions

      * true_PVs_nTracks: 
          The number of tracks originating from the true PV used to compute diff(true_PVs_nTracks)

      * true_PVs_z: 
          Z position of the true PVs used to assign the correct true PV to true_PVs_nTracks

      * use_reco_res: 
          bool to switch from expected reso from Upgrade TDR, or resolution from predicted hist FWHM

      * nsig_res_FWHM: 
          Empirical value representing the number of sigma wrt to the std resolution 
          as a function of FWHM

      * min_res: 
          Minimal resolution value (in terms of bins) for the search window - such as 3

      * threshold: 
          The threshold for considering an "on" value - such as 1e-2

      * integral_threshold: 
          The total integral required to trigger a hit - such as 0.2

      * min_width: 
          The minimum width (in bins) of a feature - such as 2

      * debug: 
          flag to print output for debugging purposes


    Ouputs: 
        ValueSet(S, Sp, MT, FP)

    This algorithm computes the weighted mean, and uses that.
    This avoids small fluctionations in the input array by requiring .
    a minium total integrated value required to "turn it on"
    (integral_threshold=0.2) and min_width of 3 bins wide.
    """

    return ValueSet_res(
        *numba_efficiency_res(
            truth, predict, true_PVs_nTracks, true_PVs_z, 
            nsig_res_FWHM, steps_extrapolation, ratio_max, 
            f_ratio_window, nbins_lookup,
            min_res, threshold, integral_threshold, min_width, debug
        )
    )
#####################################################################################
