#### adapted from Will's autoencoder_models file (https://github.com/Haxxardoux/pv-finder/blob/master/model/autoencoder_models.py)

import torch
import torch.nn.functional as F
from torch import nn

from functools import partial

# swish custom activation
class Swish(torch.autograd.Function):
    @staticmethod
    def forward(ctx, i):
        result = i*i.sigmoid()
        ctx.save_for_backward(result,i)
        return result

    @staticmethod
    def backward(ctx, grad_output):
        result,i = ctx.saved_variables
        sigmoid_x = i.sigmoid()
        return grad_output * (result+sigmoid_x*(1-result))


class Swish_module(nn.Module):
    def forward(self,x):
        return swish(x)
    
class ConvBNrelu(nn.Sequential):
    """convolution => [BN] => ReLU"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super(ConvBNrelu, self).__init__(
        nn.Conv1d(in_channels, out_channels, kernel_size, stride=1, padding=(kernel_size-1)//2),
        nn.BatchNorm1d(out_channels),
        nn.ReLU(),
        nn.Dropout(p)
#         Swish_module(),
)

class ResConvBNrelu(nn.Module):
    """convolution => [BN] => ReLU => inplace addition of input"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super().__init__()
        assert k_size % 1 == 0, "even number kernel sizes will cause shape mismatch"
        self.resblock = nn.Sequential(
            nn.Conv1d(in_channels, out_channels, kernel_size, stride=1, padding=(kernel_size-1)//2),
            nn.BatchNorm1d(out_channels),
#             Swish_module(),
            nn.ReLU(),
            nn.Dropout(p)
        )

    def forward(self, x):
        return self.resblock(x)+x


class ResUp(nn.Sequential):
    """transpose convolution => convolution => [BN] => ReLU => inplace addition of input"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super().__init__(
            nn.ConvTranspose1d(in_channels, out_channels, 2, 2),
            ResConvBNrelu(out_channels, out_channels, kernel_size=kernel_size, p=p))
        
class Convrelu(nn.Sequential):
    """convolution => ReLU"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super(Convrelu, self).__init__(
        nn.Conv1d(in_channels, out_channels, kernel_size, padding=(kernel_size-1)//2),
        nn.ReLU(),
        nn.Dropout(p))

class ConvBNreluDouble(nn.Sequential):
    """convolution => [BN] => ReLU => convolution => [BN] => ReLU"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super(ConvBNreluDouble, self).__init__(
            ConvBNrelu(in_channels, out_channels, kernel_size, p),
            ConvBNrelu(out_channels, out_channels, kernel_size, p))

class UpnoBN(nn.Sequential):
    """transpose convolution => convolution => ReLU"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super().__init__(
            nn.ConvTranspose1d(in_channels, out_channels, 2, 2),
            Convrelu(out_channels, out_channels, kernel_size=kernel_size, p=0))

class Up(nn.Sequential):
    """transpose convolution => convolution => [BN] => ReLU"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super().__init__(
            nn.ConvTranspose1d(in_channels, out_channels, 2, 2),
            ConvBNrelu(out_channels, out_channels, kernel_size=kernel_size, p=p))

class LongUp(nn.Sequential):
    """transpose convolution => ReLU convolution => [BN] => ReLU => convolution => [BN] => ReLU"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super().__init__(
            nn.ConvTranspose1d(in_channels, out_channels, kernel_size=2, stride=2),
            nn.ReLU(),
            ConvBNreluDouble(out_channels, out_channels, kernel_size=kernel_size, p=p))

        
downsample_options = {
    'ConvBNrelu':ConvBNrelu,
    'ResConvBNrelu':ResConvBNrelu,
    'ConvBNreluDouble':ConvBNreluDouble,
    'Convrelu':Convrelu,
}

upsample_options = {
    'LongUp':LongUp,
    'Up':Up,
    'UpnoBN':UpnoBN,
}

# ======================================================================
# U-Net Models
# ======================================================================
def combine(x, y, mode='concat'):
    if mode == 'concat':
        ret = torch.cat([x, y], dim=-2)
        return ret
    
    elif mode == 'add':
        return x+y
    else:
        raise RuntimeError(f'''Invalid option {mode} from choices 'concat' or 'add' ''')
    
    
# ======================= UNet ==================================== #
class UNet(nn.Module):
    def __init__(self,
                 n=32,
                 sc_mode='concat',
                 dropout_p=0,
                 d_selection='ConvBNrelu',
                 u_selection='Up',
                 n_features=4
                ):
        super().__init__()
        if sc_mode == 'concat': 
            factor = 2
        else: 
            factor = 1
        self.mode = sc_mode
        self.p = dropout_p
        
        assert d_selection in downsample_options.keys(), f'Selection for downsampling block {d_selection} not present in available options - {downsample_options.keys()}'
        assert u_selection in upsample_options.keys(), f'Selection for downsampling block {u_selection} not present in available options - {upsample_options.keys()}'
        
        d_block = downsample_options[d_selection]
        u_block = upsample_options[u_selection]
                
        self.rcbn1 = d_block(n_features, n, kernel_size = 25, p=dropout_p) # change to 2 if only KDEA and KDEB
        self.rcbn2 = d_block(n, n, kernel_size = 7, p=dropout_p)
        self.rcbn3 = d_block(n, n, kernel_size = 5, p=dropout_p)
        self.rcbn4 = d_block(n, n, kernel_size = 5, p=dropout_p)
        self.rcbn5 = d_block(n, n, kernel_size = 5, p=dropout_p)

        self.up1 = u_block(n, n, kernel_size = 5, p=dropout_p)
        self.up2 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
        self.up3 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
        self.up4 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
        self.out_intermediate = nn.Conv1d(n*factor, n, 5, padding=2)
        self.outc = nn.Conv1d(n, 1, 5, padding=2)
        
        self.d = nn.MaxPool1d(2)

    def forward(self, x):
        
        # downsampling
        x1 = self.rcbn1(x) # 12000
        temp = self.rcbn2(x1)
        x2 = self.d(temp) # 6000
        temp = self.rcbn3(x2)
        x3 = self.d(temp) # 3000
        temp = self.rcbn4(x3)
        x4 = self.d(temp) # 1500
        temp = self.rcbn5(x4)
        x = self.d(temp) # 750 

        x = self.up1(x) # 1500
        temp = combine(x, x4, mode=self.mode)
        x = self.up2(temp) # 3000
        temp = combine(x, x3, mode=self.mode)
        x = self.up3(temp) # 6000
        temp = combine(x, x2, mode=self.mode)
        x = self.up4(temp) # 12000
        temp = combine(x, x1, mode=self.mode)
        x = self.out_intermediate(temp) # 12000
        logits_x0 = self.outc(x)

        ret = F.softplus(logits_x0).squeeze()
        return  ret
    
    
    # Fuse Conv+BN and Conv+BN+Relu modules prior to quantization
    # This operation does not change the numerics
    def fuse_model(self):
        for m in self.modules():
            if type(m) == ConvBNrelu:
                torch.quantization.fuse_modules(m, ['0', '1', '2'], inplace=True)


# ======================= UNetPlusPlus ==================================== #
class UNetPlusPlus(nn.Module):
    def __init__(self,
                 n=64,
                 sc_mode='concat',
                 dropout_p=.25,
                 d_selection='ConvBNrelu',
                 u_selection='Up',
                 n_features=4
                ):
        super().__init__()
        if sc_mode == 'concat': 
            factor = 2
        else: 
            factor = 1
        self.mode = sc_mode
        self.p = dropout_p
        
        assert d_selection in downsample_options.keys(), f'Selection for downsampling block {d_selection} not present in available options - {downsample_options.keys()}'
        assert u_selection in upsample_options.keys(), f'Selection for downsampling block {u_selection} not present in available options - {upsample_options.keys()}'
        
        d_block = downsample_options[d_selection]
        u_block = upsample_options[u_selection]
                
        self.rcbn1 = d_block(n_features, n, kernel_size = 25, p=dropout_p)
        self.rcbn2 = d_block(n, n, kernel_size = 7, p=dropout_p)
        self.rcbn3 = d_block(n, n, kernel_size = 5, p=dropout_p)
        self.rcbn4 = d_block(n, n, kernel_size = 5, p=dropout_p)
        self.rcbn5 = d_block(n, n, kernel_size = 5, p=dropout_p)

        self.ui = nn.ConvTranspose1d(n, n, 2, 2)
        self.i1 = ConvBNrelu(2*n, n, kernel_size=5, p=dropout_p)
        self.i2 = ConvBNrelu(2*n, n, kernel_size=5, p=dropout_p)
        self.i3 = ConvBNrelu(2*n, n, kernel_size=5, p=dropout_p)
        self.i4 = ConvBNrelu(3*n, n, kernel_size=5, p=dropout_p)
        self.i5 = ConvBNrelu(3*n, n, kernel_size=5, p=dropout_p)
        self.i6 = ConvBNrelu(4*n, n, kernel_size=5, p=dropout_p)
        
        self.up1 = nn.ConvTranspose1d(n, n , 2, 2)
        self.up_c1 = ConvBNrelu(2*n, n, kernel_size=5, p=dropout_p)
        self.up2 = nn.ConvTranspose1d(n, n, 2, 2)
        self.up_c2 = ConvBNrelu(3*n, n, kernel_size=5, p=dropout_p)
        self.up3 = nn.ConvTranspose1d(n, n, 2, 2)
        self.up_c3 = ConvBNrelu(4*n, n, kernel_size=5, p=dropout_p)
        self.up4 = nn.ConvTranspose1d(n, n, 2, 2)
        self.up_c4 = ConvBNrelu(5*n, n, kernel_size=5, p=dropout_p)
        
        self.out_intermediate = nn.Conv1d(2*n, n, 5, padding=2) # padding=5-1//2
        self.outc = nn.Conv1d(n, 1, 5, padding=2) # padding=5-1//2
        
        self.d = nn.MaxPool1d(2)

    def forward(self, x):
        
        # down-sampling
        d1 = self.rcbn1(x) # 12000
        d2 = self.d(self.rcbn2(d1)) # 6000
        d3 = self.d(self.rcbn3(d2)) # 3000
        d4 = self.d(self.rcbn4(d3)) # 1500
        d5 = self.d(self.rcbn5(d4)) # 750
        
        # dense skip connections
        ui1 = self.ui(d2)
        ui2 = self.ui(d3)
        ui3 = self.ui(d4)
        
        i1 = self.i1(torch.cat([ui1, d1], dim=1))
        i2 = self.i2(torch.cat([ui2, d2], dim=1))
        i3 = self.i3(torch.cat([ui3, d3], dim=1))
        
        ui4 = self.ui(i2)
        ui5 = self.ui(i3)
        
        i4 = self.i4(torch.cat([ui4, d1, i1], dim=1))
        i5 = self.i5(torch.cat([ui5, d2, i2], dim=1))
        
        ui6 = self.ui(i5)
        
        i6 = self.i6(torch.cat([ui6, d1, i1, i4], dim=1))
        
        # up-sampling
        u1 = self.up_c1(torch.cat([d4, self.up1(d5)], dim=1))             # 1500
        u2 = self.up_c2(torch.cat([d3, i3, self.up2(u1)], dim=1))         # 3000
        u3 = self.up_c3(torch.cat([d2, i2, i5, self.up1(u2)], dim=1))     # 6000
        u4 = self.up_c4(torch.cat([d1, i1, i4, i6, self.up1(u3)], dim=1)) # 12000
        
        x = self.out_intermediate(torch.cat([u4, d1], dim=1))
        logits_x0 = self.outc(x)

        ret = F.softplus(logits_x0).squeeze()
        return  ret
    
    
    # Fuse Conv+BN and Conv+BN+Relu modules prior to quantization
    # This operation does not change the numerics
    def fuse_model(self):
        for m in self.modules():
            if type(m) == ConvBNrelu:
                torch.quantization.fuse_modules(m, ['0', '1', '2'], inplace=True)

                
                
# ======================= Perturbative UNet ==================================== #
class PerturbativeUNet(nn.Module):
    def __init__(self, args, n, sc_mode='concat', dropout_p=0):
        super().__init__()
        self.mode = sc_mode
        if sc_mode == 'concat': 
            factor = 2
        else: 
            factor = 1
        
        # network for perturbative features
        self.cbn1_x = ConvBNrelu(2, n, kernel_size = 11, p = dropout_p)
        self.cbn2_x = ConvBNrelu(n, n, p = dropout_p)
        self.cbn3_x = ConvBNrelu(n, n, p = dropout_p)
        self.cbn4_x = ConvBNrelu(n, n, p = dropout_p)
        self.up1_x = Up(n, n, p = dropout_p)
        self.up2_x = Up(n*factor, n, p = dropout_p)
        self.up3_x = Up(n*factor, n, p = dropout_p)
        self.up4_x = Up(n*factor, n, p = dropout_p)

        self.down = nn.MaxPool1d(2)
        self.d = nn.MaxPool1d(2)
        
        # network for X features
        self.rcbn1 = ConvBNrelu(1, n, kernel_size = 25, p = dropout_p)
        self.rcbn2 = ConvBNrelu(n, n, kernel_size = 7, p = dropout_p)
        self.rcbn3 = ConvBNrelu(n, n, kernel_size = 5, p = dropout_p)
        self.rcbn4 = ConvBNrelu(n, n, kernel_size = 5, p = dropout_p)
        self.rcbn5 = ConvBNrelu(n, n, kernel_size = 5, p = dropout_p)

        self.up1 = Up(n, n, kernel_size = 5, p = dropout_p)
        self.up2 = Up(n*factor, n, kernel_size = 5, p = dropout_p)
        self.up3 = Up(n*factor, n, kernel_size = 5, p = dropout_p)
        self.up4 = Up(n*factor, n, kernel_size=5, p = dropout_p)
        self.out_intermediate = nn.Conv1d(n*factor, n, 5, padding=2)

        self.outc_larger = nn.Conv1d(factor*n, 1, 3, padding=1)

    def forward(self, x):
        X = x[:, 0:1, :] # one-slice prevents need for .unsqueeze()
        x_y = x[:, -2:, :]
        
        # x / y  feature
        p_x = self.cbn1_x(x_y) 
        p_x = self.down(p_x)

        p_x2 = self.cbn2_x(p_x)
        p_x = self.down(p_x2)
        
        p_x3 = self.cbn3_x(p_x)
        p_x = self.down(p_x3)

        p_x4 = self.cbn4_x(p_x)
        p_x = self.down(p_x4)

        p_x = self.up1_x(p_x)
        p_x = self.up2_x(combine(p_x, p_x4, mode=self.mode))
        p_x = self.up3_x(combine(p_x, p_x3, mode=self.mode))
        logits_x1 = self.up4_x(combine(p_x, p_x2, mode=self.mode))

        # X feature based on U-Net (parallel network)
        x1 = self.rcbn1(X) # 4000
        x2 = self.d(self.rcbn2(x1)) # 2000
        x3 = self.d(self.rcbn3(x2)) # 1000
        x4 = self.d(self.rcbn4(x3)) # 500
        x = self.d(self.rcbn5(x4)) # 250

        x = self.up1(x) # 500
        x = self.up2(combine(x, x4, mode=self.mode)) # 1000
        x = self.up3(combine(x, x3, mode=self.mode)) # 2000
        x = self.up4(combine(x, x2, mode=self.mode)) # 4000
        logits_x0 = self.out_intermediate(combine(x, x1, mode=self.mode))

        logits_X_and_x = self.outc_larger(combine(logits_x0, logits_x1, mode=self.mode))

        ret = F.softplus(logits_X_and_x).squeeze(1)
        return  ret