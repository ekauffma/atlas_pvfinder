#### PERTURBATIVE UNET INSTRUCTIONS ####
### Step 1: load only KDE-A and KDE-B and train using UNet until convergence
### Step 2: load all four features and load weights from previous model into Perturbative UNet and freeze them. train using PerturbativeUNet until convergence
### Step 3: unfreeze all weights and train using Perturbative UNet until convergence

import torch
import mlflow
import time
import pickle

from model.alt_loss_A import Loss
from model.training import trainNet, select_gpu
from model.utilities import load_full_state, count_parameters, Params, save_to_mlflow
from pathlib import Path

from model.autoencoder_models import UNet
from model.autoencoder_models import UNetPlusPlus
from model.autoencoder_models import DenseNet as DenseNet
from model.collectdata_poca_KDE import collect_data_poca_ATLAS as collect_data_poca
from model.autoencoder_models import PerturbativeUNet
from model.autoencoder_models import UNet

import torch
import torch.nn.functional as F
from torch import nn

from functools import partial

import traceback
import sys

# edit parameters below as necessary
args = Params(
    batch_size=64,
    device=select_gpu(0),
    epochs=4000,
    lr=1e-5,
    experiment_name='ATLAS Perturbative UNet',
    asymmetry_parameter=0.0,
    run_name='MyRun'
)

# change to path of desired hdf5 file
file = '/share/lazy/ekauffma/kernel_atlas_220817_b.h5'

# be sure to save the below indices so that you can use a distinct set of events for testing
indices = np.arange(0,events,1,dtype=int)
np.random.shuffle(indices)
# pickle.dump(indices,open("indices.p","wb"))

events = 50000
## This is used when training with the new KDE
train_loader = collect_data_poca(file,
                                 batch_size=args['batch_size'],
                                 device=args['device'], 
                                 shuffle=False,
                                 load_A_and_B=True,
                                 load_xy=False, # false for step 1, true for steps 2 and 3
                                 indices = indices[0:40000],
                                 masking = True,
                                 )

torch.save(train_loader, "train_loader.pt")

val_loader = collect_data_poca(file,
                               batch_size=args['batch_size'],
                               device=args['device'],
                               shuffle=False,
                               load_A_and_B=True,
                               load_xy=False, # false for step 1, true for steps 2 and 3
                               indices = indices[40000:50000]),
                               masking = True,
                               )
    
torch.save(val_loader, "val_loader.pt")

train_loader = torch.load("train_loader.pt", map_location = args["device"])
val_loader = torch.load("val_loader.pt", map_location = args["device"])

mlflow.tracking.set_tracking_uri('file:/share/lazy/pv-finder_model_repo')
mlflow.set_experiment(args['experiment_name'])

# model = PerturbativeUNet() # steps 2 and 3
model = UNet(n=64) # step 1

## use when loading pre-trained weights (steps 2 and 3). change to path of trained model
#pretrained_model = torch.load('/share/lazy/pv-finder_model_repo/38/2d79825f987d464691cb3b83b35d8e3d/artifacts/run_stats.pyt')

### weight freezing/unfreezing start ### (uncomment for steps 2 and 3)
# ct = 0
# for child in model.children():
#     print('ct, child = ',ct, "  ", child)
#     if ct < 12:
#         print("     About to set param.requires_grad=True for ct = ", ct, "params")
#         for param in child.parameters():
#             param.requires_grad = True  # false for step 2, true for step 3
#     ct += 1
### weight freezing end ###

# pretrained_dict = pretrained_model.state_dict() # steps 2 and 3
model_dict = model.state_dict()

### optional printing start ###
# print("for model_dict")
# index = 0
# for k,v in model_dict.items():
#     print("index, k =  ",index,"  ",k)
#     index = index+1
    
# print(" \n","  for pretrained_dict")
# index = 0
# for k,v in pretrained_dict.items():
#     print("index, k =  ",index,"  ",k)
#     index = index+1
### optional printing end ###

# pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict} # step 2 and 3
## overwrite entries in the existing state dict
# model_dict.update(pretrained_dict) # step 2 and 3


## load the new state dict
##   need to use strict=False as the two models state model attributes do not agree exactly
##   see https://pytorch.org/docs/master/_modules/torch/nn/modules/module.html#Module.load_state_dict
# model.load_state_dict(pretrained_dict,strict=False) # step 2 and 3

model_dict = model.state_dict()

# sometimes need to directly edit initial weights, because the training in step 2 doesn't always easily converge
# factor = 0.001
#model_dict["cbn1_x.0.bias"] = factor*model_dict["cbn1_x.0.bias"]
#model_dict["cbn1_x.0.weight"] = factor*model_dict["cbn1_x.0.weight"]
#model_dict["cbn1_x.1.bias"] = factor*model_dict["cbn3_x.1.bias"]
#model_dict["cbn1_x.1.weight"] = factor*model_dict["cbn3_x.1.weight"]
#model_dict["cbn2_x.0.bias"] = factor*model_dict["cbn2_x.0.bias"]
#model_dict["cbn2_x.0.weight"] = factor*model_dict["cbn2_x.0.weight"]
#model_dict["cbn2_x.1.bias"] = factor*model_dict["cbn2_x.1.bias"]
#model_dict["cbn2_x.1.weight"] = factor*model_dict["cbn2_x.1.weight"]
#model_dict["cbn3_x.0.bias"] = factor*model_dict["cbn3_x.0.bias"]
#model_dict["cbn3_x.0.weight"] = factor*model_dict["cbn3_x.0.weight"]
#model_dict["cbn3_x.1.bias"] = factor*model_dict["cbn3_x.1.bias"]
#model_dict["cbn3_x.1.weight"] = factor*model_dict["cbn3_x.1.weight"]
#model_dict["cbn4_x.0.bias"] = factor*model_dict["cbn4_x.0.bias"]
#model_dict["cbn4_x.0.weight"] = factor*model_dict["cbn4_x.0.weight"]
#model_dict["cbn4_x.1.bias"] = factor*model_dict["cbn4_x.1.bias"]
#model_dict["cbn4_x.1.weight"] = factor*model_dict["cbn4_x.1.weight"]

#model_dict["up1_x.0.bias"] = factor*model_dict["up1_x.0.bias"]
#model_dict["up1_x.0.weight"] = factor*model_dict["up1_x.0.weight"]
#model_dict["up1_x.1.0.bias"] = factor*model_dict["up1_x.1.0.bias"]
#model_dict["up1_x.1.0.weight"] = factor*model_dict["up1_x.1.0.weight"]
#model_dict["up1_x.1.1.bias"] = factor*model_dict["up1_x.1.1.bias"]
#model_dict["up1_x.1.1.weight"] = factor*model_dict["up1_x.1.1.weight"]
#model_dict["up2_x.0.bias"] = factor*model_dict["up2_x.0.bias"]
#model_dict["up2_x.0.weight"] = factor*model_dict["up2_x.0.weight"]
#model_dict["up2_x.1.0.bias"] = factor*model_dict["up2_x.1.0.bias"]
#model_dict["up2_x.1.0.weight"] = factor*model_dict["up2_x.1.0.weight"]
#model_dict["up2_x.1.1.bias"] = factor*model_dict["up2_x.1.1.bias"]
#model_dict["up2_x.1.1.weight"] = factor*model_dict["up2_x.1.1.weight"]
#model_dict["up3_x.0.bias"] = factor*model_dict["up3_x.0.bias"]
#model_dict["up3_x.0.weight"] = factor*model_dict["up3_x.0.weight"]
#model_dict["up3_x.1.0.bias"] = factor*model_dict["up3_x.1.0.bias"]
#model_dict["up3_x.1.0.weight"] = factor*model_dict["up3_x.1.0.weight"]
#model_dict["up3_x.1.1.bias"] = factor*model_dict["up3_x.1.1.bias"]
#model_dict["up3_x.1.1.weight"] = factor*model_dict["up3_x.1.1.weight"]
#model_dict["up4_x.0.bias"] = factor*model_dict["up4_x.0.bias"]
#model_dict["up4_x.0.weight"] = factor*model_dict["up4_x.0.weight"]
#model_dict["up4_x.1.0.bias"] = factor*model_dict["up4_x.1.0.bias"]
#model_dict["up4_x.1.0.weight"] = factor*model_dict["up4_x.1.0.weight"]
#model_dict["up4_x.1.1.bias"] = factor*model_dict["up4_x.1.1.bias"]
#model_dict["up4_x.1.1.weight"] = factor*model_dict["up4_x.1.1.weight"]

model.load_state_dict(model_dict,strict=False)


optimizer = torch.optim.Adam(model.parameters(), lr=args['lr'])
loss = Loss(epsilon=1e-5,coefficient=args['asymmetry_parameter'])

parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)

# ## variables for avg eff and fp over the last few epochs for comparison
avgEff = 0.0
avgFP = 0.0

# ## move model to GPU/device
model = model.to(args['device'])

# ## tune kernel based on gpu
# #torch.backends.cudnn.benchmark=True

# use model if step 1, pretrained
train_iter = enumerate(trainNet(model, optimizer, loss, train_loader, val_loader, args['epochs'], notebook=False))
with mlflow.start_run(run_name = args['run_name']) as run:
    mlflow.log_artifact('TrainATLAS_script_punet.py')
    for i, result in train_iter:
        print(result.cost)
        torch.save(model, 'run_stats.pyt')
        mlflow.log_artifact('run_stats.pyt')

        ## save each epoch's model state dictionary to separate folder
        ## use to load weights from specific epoch (choose using mlflow)
        #output = '/share/lazy/pv-finder_model_repo/ML/' + args['run_name'] + '_' + str(result.epoch) + '.pyt'
        #torch.save(model, output)
        #mlflow.log_artifact(output)

        ### find average eff and fp over last 10 epochs ###
        ## If we are on the last 10 epochs but NOT the last epoch
        if(result.epoch >= args['epochs']-10):
            avgEff += result.eff_val.eff_rate
            avgFP += result.eff_val.fp_rate

        ## If we are on the last epoch
        if(result.epoch == args['epochs']-1):
            print('Averaging...\n')
            avgEff/=10
            avgFP/=10
            mlflow.log_metric('10 Eff Avg.', avgEff)
            mlflow.log_metric('10 FP Avg.', avgFP)
            print('Average Eff: ', avgEff)
            print('Average FP Rate: ', avgFP)
        
        ## save results to mlflow after each epoch
        save_to_mlflow({
            'Metric: Training loss':result.cost,
            'Metric: Validation loss':result.val,
            'Metric: Efficiency':result.eff_val.eff_rate,
            'Metric: False positive rate':result.eff_val.fp_rate,
            'Param: Parameters':parameters,
            'Param: Asymmetry':args['asymmetry_parameter'],
            'Param: Batch Size':args['batch_size'],
            'Param: Epochs':args['epochs'],
            'Param: Learning Rate':args['lr'],
        }, step=i)
            
