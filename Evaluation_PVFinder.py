# gets info from pv-finder output needed to generate performance plots
from model.collectdata_poca_KDE import collect_data_poca_ATLAS
from model.collectdata_poca_KDE import collect_truth_ATLAS
from model.efficiency import efficiency
from model.efficiency_res_optimized_atlas import filter_nans_res, efficiency_res, ValueSet_res, pv_locations_updated_res, get_reco_resolution, get_PVs_label, compare_res_reco

import sys
import argparse
import numpy as np
import pickle
import uproot

# parameters for LHCb-style efficiency
PARAM_EFF = {
    "difference": 5.0, # maximum # bins between truth and predicted
    "threshold": 1e-2, # start counting towards reconstructed PV when over this value
    "integral_threshold": 0.2, # integral of reconstructed PV distribution must be over this value to be valid
    "min_width": 3, # number of bins required for reconstructed PV to be valid
}

def main(dir_name, which_model, path_hdf5, path_root, nevents, index_path, sigma_vtx_vtx): 
    
    start = int(0.95*nevents)
    end = nevents

    # get indices (use same shuffle as when running TestModel.py)
    indices = pickle.load(open(index_path, 'rb'))[start:end]

    # get truth pv information
    truth = collect_truth_ATLAS(path_hdf5, indices = indices)
    print("truth collected")
    
    # load inputs, labels, outputs obtained from running TestModel.py
    inputs = pickle.load(open(f'{dir_name}/inputs_pvf_fullcov_{which_model}.p', 'rb'))
    labels = pickle.load(open(f'{dir_name}/labels_pvf_fullcov_{which_model}.p', 'rb'))
    outputs = pickle.load(open(f'{dir_name}/outputs_pvf_fullcov_{which_model}.p', 'rb'))
    
    # binning info
    totalNumBins = 12000
    zMax=240
    zMin=-240
    binwidth = (zMax-zMin)/totalNumBins
    zvals = np.linspace(zMin, zMax, totalNumBins, endpoint=False) + binwidth/2
    zbins = np.linspace(zMin,zMax,totalNumBins)
    bins_1mm = totalNumBins/(zMax - zMin)
    
    ####### define efficiency parameters #######
    threshold = 1e-2 # above this value, start calculating predicted PV
    integral_threshold = 0.2 # require this integral or above when finding predicted PV
    min_width = 3 # require predicted PV to be at least this number of bins wide

    nsig_res_FWHM = 5 # number of sigma to calculate predicted peak resolution
    steps_extrapolation = 10 # number of steps to extrapolate between bin values when calculating the FWHM
    ratio_max = 0.5 # at what fraction of the max to calculate the width (usually half)

    
    nevts = end-start
    
    # initialize arrays to keep number of reconstructed vertices in
    reco_merged = np.zeros(nevts)
    reco_split = np.zeros(nevts)
    reco_clean = np.zeros(nevts)
    reco_fake = np.zeros(nevts)
    event_efficiency = np.zeros(nevts)
    
    # list to contain distances between reconstructed PVs
    predicted_pv_distances = []

    # for calculating efficiency as a function of ntrks
    truth_correct = []
    truth_ntrks = []
    
    # for lhcb-style efficiency
    S_total=0
    Sp_total=0
    MT_total=0
    FP_total=0
    
    total_reco_z = []
    for iEvt in range(nevts):
    
        print("iEvt = ", iEvt)

        # get current neural network info
        outputs_current = outputs[iEvt]
        inputs_current  = inputs[iEvt]
        labels_current  = labels[iEvt]

        # get current truth info
        sortind = np.argsort(truth.z[iEvt])
        truth_n_current = truth.n[iEvt][sortind]
        truth_x_current = truth.x[iEvt][sortind]
        truth_y_current = truth.y[iEvt][sortind]
        truth_z_current = truth.z[iEvt][sortind]

        # get locations of predited PVs
        predict_loc, predict_peakloc, predict_cleft, predict_cright = pv_locations_updated_res(outputs_current, 
                                                                                               threshold, 
                                                                                               integral_threshold,
                                                                                               min_width)    

        total_reco_z.extend(predict_loc)
        # get distances between nearby vertices
        predict_loc_mm = predict_loc*binwidth+zMin
        np.random.shuffle(predict_loc_mm)
        for i in range(len(predict_loc_mm)-1):
            for j in range(i+1, len(predict_loc_mm)):
                predicted_pv_distances.append(predict_loc_mm[i]-predict_loc_mm[j])

                
        # whether each predicted PV is valid (masked == False)
        validinds = filter_nans_res(predict_loc, labels_current)
        validinds = list(range(len(predict_loc)))  #uncomment if you do not want to filter


        # update predicted PV list
        filtered_predict_loc = predict_loc[validinds]
        filtered_predict_peakloc = predict_peakloc[validinds]
        filtered_predict_cleft = predict_cleft[validinds]
        filtered_predict_cright = predict_cright[validinds]

        # constant resolution: 
        reco_res = sigma_vtx_vtx*np.ones(len(filtered_predict_loc))
        
        # consider truth vertices with more than 1 associated track
        truth_z_bins_valid = (truth_z_current[truth_n_current>=2] - zMin) / binwidth

        # count reconstructed vertices and return related info
        current_result, truth_classification, localdensity = compare_res_reco(truth_z_bins_valid, filtered_predict_loc, reco_res, 0)
        
        # calculate lhcb-style efficiency
        eff = efficiency(labels_current, outputs_current, **PARAM_EFF)
        S_total+=eff[0]
        Sp_total+=eff[1]
        MT_total+=eff[2]
        MT_total+=eff[3]

        # truth efficiency info
        truth_correct_event = []
        truth_n_current_filtered = truth_n_current[truth_n_current>=2]
        for i in range(len(truth_classification)):
            truth_ntrks.append(truth_n_current_filtered[i])
            if "clean" in truth_classification[i] or "merged" in truth_classification[i]:
                truth_correct.append(1)
                truth_correct_event.append(1)
            else:
                truth_correct.append(0)
                truth_correct_event.append(0)

        print(current_result)

        reco_merged[iEvt] = current_result.reco_merged
        reco_split[iEvt] = current_result.reco_split
        reco_clean[iEvt] = current_result.reco_clean
        reco_fake[iEvt] = current_result.reco_fake
        event_efficiency[iEvt] = sum(truth_correct_event)/len(truth_correct_event)
    
    # load pileup information from ROOT tree. be careful to ensure that the indices correspond to the data loaded above
    PVFinderData = uproot.open(path_root)["PVFinderData"]
    ActualNumOfInt = list(PVFinderData["ActualNumOfInt"].array()[indices])
    
    # dictionaries with key corresponding to pileup and values = lists of numbers of clean/split/merged/fake from different events
    separated_clean = {}
    separated_merged = {}
    separated_split = {}
    separated_fake = {}
    separated_all = {}
    separated_eff = {}

    for i in range(len(reco_clean)):
        if not np.rint(ActualNumOfInt[i]) in separated_clean.keys():
            separated_clean[np.rint(ActualNumOfInt[i])] = []
        separated_clean[np.rint(ActualNumOfInt[i])].append(reco_clean[i])

        if not np.rint(ActualNumOfInt[i]) in separated_merged.keys():
            separated_merged[np.rint(ActualNumOfInt[i])] = []
        separated_merged[np.rint(ActualNumOfInt[i])].append(reco_merged[i])

        if not np.rint(ActualNumOfInt[i]) in separated_split.keys():
            separated_split[np.rint(ActualNumOfInt[i])] = []
        separated_split[np.rint(ActualNumOfInt[i])].append(reco_split[i])

        if not np.rint(ActualNumOfInt[i]) in separated_fake.keys():
            separated_fake[np.rint(ActualNumOfInt[i])] = []
        separated_fake[np.rint(ActualNumOfInt[i])].append(reco_fake[i])

        if not np.rint(ActualNumOfInt[i]) in separated_all.keys():
            separated_all[np.rint(ActualNumOfInt[i])] = []
        separated_all[np.rint(ActualNumOfInt[i])].append(reco_fake[i] + reco_clean[i] + reco_merged[i] + reco_split[i])
        
        if not np.rint(ActualNumOfInt[i]) in separated_eff.keys():
            separated_eff[np.rint(ActualNumOfInt[i])] = []
        separated_eff[np.rint(ActualNumOfInt[i])].append(event_efficiency[i])
        
    #save these dictionaries
    pickle.dump(separated_all, open(f"{dir_name}/separated_all_pvf_{which_model}.p","wb"))
    pickle.dump(separated_clean, open(f"{dir_name}/separated_clean_pvf_{which_model}.p","wb"))
    pickle.dump(separated_merged, open(f"{dir_name}/separated_merged_pvf_{which_model}.p","wb"))
    pickle.dump(separated_split, open(f"{dir_name}/separated_split_pvf_{which_model}.p","wb"))
    pickle.dump(separated_fake, open(f"{dir_name}/separated_fake_pvf_{which_model}.p","wb"))
    pickle.dump(separated_eff, open(f"{dir_name}/separated_eff_pvf_{which_model}.p","wb"))
    
    
    
    print("Efficiency = ",sum(truth_correct)/len(truth_correct))
    print("FPR = ",np.average(reco_fake))
    print("Total Length = ", len(truth_correct))
    
    print("LHCb-style Efficiency = ", S_total/(S_total+MT_total))
    print("LHCb-style FalsePos = ", FP_total/len(indices))
        
    # save predicted truth efficiency and ntrk info
    pickle.dump(truth_correct, open(f"{dir_name}/truth_correct_pvf_{which_model}.p","wb"))
    pickle.dump(truth_ntrks, open(f"{dir_name}/truth_ntrks_pvf_{which_model}.p","wb"))
    pickle.dump(total_reco_z, open(f"{dir_name}/total_reco_z_{which_model}.p","wb"))
    
    pickle.dump(predicted_pv_distances, open(f"{dir_name}/predicted_pv_distances_pvf_{which_model}.p","wb"))



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This program finds the number of reconstructed vertices in each category for a PV-Finder model and saves the outputs')
    parser.add_argument("-n","--nevents", help="Number of events in input file (95% of these are used for training)", default=51000, type=int)
    parser.add_argument("-i","--indices", help="Pickled numpy array containing the indices to use for consistent train/test split (use same file as for training)", required=True)
    parser.add_argument("-o", "--dirname", help="directory to load pv-finder results from", type=str, required=True)
    parser.add_argument("-m", "--modelname", help="name of model architecture (i.e. unet or unetplusplus)", type=str, required=True)
    parser.add_argument("-f", "--path_hdf5", help="path to input hdf5 file", type=str, required=True)
    parser.add_argument("-r", "--path_root", help="path to input root file (the same file used to generate the hdf5 file)", type=str, required=True)
    parser.add_argument("-s", "--sigma", help="value of sigma_vtx_vtx to use", type=float, required=True)
    
    args = parser.parse_args()
    
    main(args.dirname, args.modelname, args.path_hdf5, args.path_root, 
         args.nevents, args.indices, args.sigma)
